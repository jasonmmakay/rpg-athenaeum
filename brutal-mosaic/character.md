# What is a Man?

<img src="imgs/character.jpg"
  style="padding:0;display:block;margin:0 auto;max-height:100%;max-width:100%;">

**This module is Mosaic Strict**

## Traits
`Traits` are short, descriptive phrases that may provide mechanical bonuses,
penalties or narative effects depending on the situation, but always inform a
character's actions or an item's properties. Traits could describe their
personility (*iron willed*, *cowardly*), innate qualities (*weakness to fire*,
*fragile*), or reflect their reputation (*feared among nobility*).


## Spheres, Grit and Talent
The `three spheres` are broad catagories where a character could find themselves
in a conflict. Each sphere has two attributes: `grit` measures the character's
ability to resist damage and endure punishment of certain types before
succumbing; `talent` measures their creativeness, determining a pool of points
that fuel extraordinary abilities and powers. If a sphere's talent pool runs
out, a character may make the drastic decision to burn grit instead.

As each sphere has its own name, its grit and talent attributes do, too:

| Sphere     | Grit      | Talent      |
| :--------- | :-------- | :---------- |
| `Physical` | `Vigor`   | `Stamina`   |
| `Mental`   | `Resolve` | `Willpower` |
| `Social`   | `Persona` | `Influence` |

Points spent from talent pools will recover quickly on their own, given some
time to rest and refocus oneself.

But time, effort and resources will be necessary to recover grit -
vigor damage may require medical treatment or mechanical repair; resolve might
necessitate deep meditation or counselling sessions for recovery; persona damage
could require owing favors or paying bribes. Though time may mend grit on its
own, it does so slowly, as wounds fester, the mind turns in on itself and the
mob refuses to forget.


## Domains and Skills
A `domain` is to a `skill` as soldier is to swordplay or sage is to astrology:
the first is akin to a job and the second an area of specialization within it.
Domains will naturally encompass many skills.

Both domains and skills are rated in six levels of mastery:

* `0` `Untrained` - You may know broadly what needs done, but have no practical
  experience in the matter.
* `1` `Novice` - You have at least attempted this a few times before.
* `2` `Trained` - You have studied and practiced the craft enough to be more help
  than hindrance.
* `3` `Professional` - You have the capability to make a living with this skill.
* `4` `Expert` - You have honed your craft to the point of being able to train
  others.
* `5` `Master` - You push the boundaries of what it even means to practice the
  craft, shaping and redefining it for those that will follow.


## Techniques
Extraordinary abilities that can only be used a limited number of times are
called `techniques`.  These might include magic spells, heroic feats of battle
prowess or a gift for extracting favors. The set of all techniques a character
can currently use is called the `repetoire`.

The relative strength of techniques are measured by their cost, in talent, to
use them: weaker techniques cost less than stronger techniques and the cost may
be drawn from multiple spheres.

Each technique must be associated with a domain or skill, and its total cost
cannot exceed the rating of its association. Furthermore, the total cost of all
techniques in the repetoire must be less than or equal to double the sum of a
character's talents.


# Character Creation and Advancement

## The Past
Create two traits that affect your character's behavior at a fundamental level,
one positive and the other negative. Referees are encouraged to reward role
playing of negative traits when it would create trouble for the character, but
should also take care not to allow traits that would derail the gaming
experience for the other players (*Lone Wolf* may be a fine trait for an NPC,
but would not be conducive to teamwork among a group of players).

Choose two novice domains and a trained skill for each that represents your
background, upbringing or heritage, describing where you came from. These are
your `influences`.

Your vigor, resolve and persona have starting values of 1; your
stamina, willpower and influence begin at 0.


## The Present
Select a domain to be trained in and choose two skills related to it at a
professional level to represent what you do to make your way in the world, day
to day. This is your `occupation`.

Distribute 12 points among your grits and 3 among your talents, considering the
types of danger your occupation might put you in and the kinds of abilities it
might grant you.

Create several techniques and associate them with one of your skills or domains,
ensuring that the limits on their strength and number are followed.


## The Future
Pursuing a new skill in a domain starts the skill at the novice level - since
it is related to your domain, you've had to exercise this skill several times
before, but not rigorously. A rank and file soldier may be called on to lead
his squad in drills, but doesn't do so every day; a lab assistant may read and
execute the procedures of an experiment designed by the lead, but has had
little opportunity to design an experiment themselves.

Occupations advance in rank when a number of their skills reach that same
numerical rank. When an occupation advances in rank, rename it to represent the
increased level of expertise and narrowing specificity of the practice - a
soldier might be promoted to a squad leader or a lab assistant to a lead
experimenter. The new name of the occupation should reflect those skills you are
most skilled in.

Each time a skill reaches the rank necessary to advance the occupation's rank,
distribute another 4 points to your grits and 1 point to your talents.

You may also modify your technique repetoire: so long as the constraints on
repetoires are obeyed, you may purchase one new technique, but may also retire
any known technique or bring any number of them out of retirement.

A character may not work ahead to earn points from an occupation ranking beyond
its next rank: for example, raising a skill to master ranking does not grant any
other benefits when its occupation's ranking is only profesional; they must wait
until their occupation advances to the expert rank to gain those other benefits.
