# Bestiary
[TOC]


## Animals
### Giant Bear

**Attributes**
- CON D10 (35) // ATN D6 (10)
- AV 3 // PAV 0 // CAV 0

**Skills**
- Bear D8

**Signatures**
- Mother's Rage: When with her cubs, a mother will never flee and will be far
more aggressive than usual.

**Attacks**
- 2x Claws or Roar
  - Claw: D6 slashing; if both hit, then Rend
  - Rend: 2D4 slashing AP 6
  - Roar: 2D4 ATN AoE near

**Treasure**
- Pelt
- Claws
- Teeth

## Insects
### Megapede
Megapedes have the appearance of common centipedes, only enlarged to gargantuan
proportions: megapedes may reach sizes up to 12 feet long. Known for their
quickness, the insects are carniverous, biting their prey and injecting them
with poison, often dragging the corpse back to its nest.

**Attributes**
- CON D8 (40) // ATN D4 (8)
- AV 4 // PAV 0 // CAV 1

**Skills**
- Predatory Hunter D8
- Dodge D6

**Signatures**
- Fast: The megapede can move a near or close distance without limitation, or
far by sacrificing its action.
- Heat/Fire/Dryness Weakness: Megapedes lack the waxy coating that many other
insects have, and they can quickly desicate if exposed to extreme heat or
dryness.
- Bludgeoning Resistence: AP negated from bludgeoning attacks.
- Brittle Carapace: bludgeoning damage will crack the carapace; a targeted
bludgeoning strike on a cracked spot will break the carapace open; all targeted
strikes at the open carapace are then at AV 0 and bludgeoning resistance is
negated in that spot.

**Attacks**
- 2x Venomous bite: 2D4 piercing AP2 + D4 poison / D4 rounds CON save

**Treasure**
- Carapace Plates

## Template
### TEMPLATE
Description

**Attributes**
- CON DX (X) // ATN DX (X)
- AV X // PAV X // CAV X

**Skills**
- Major Skill DX

**Signatures**
- X

**Attacks**
- X

**Treasure**
- X