# Icosan Calendar
The Icosan calendar consists of 13, 28 day months and a single intercalanary
day at the beginning of each year, Solstice.  Each week is 7 days long.

```
Weekdays                  Months
---------------------------------------------
1 Hedras           Solstice (single day)
2 Tetdras       1 Icewane        7 Highsun
3 Hexdras       2 Thawturn       8 Harvest
4 Octdras       3 Rainmoot       9 Goldrime
5 Nondras       4 Greenrime     10 Duskrime
6 Dodecdras     5 Planting      11 Frostrime
7 Icosadras     6 Blazemoot     12 Snowmoot
                       13 Fellnight
```
