# WAYPOINT 079539
<img src="waypoint-exterior.jpg"
  style="padding:0;display:block;margin:0 auto;max-height:100%;max-width:100%;">


The concrete building before you has been swallowed by the forest, moss and
vines cover and the trees have nearly reclaimed the space cleared round it. Its
outer walls rise in a steep slope and a single doorway yawns darkly in its
front, smashed open. To the side of the door, a placard reads in Ancient Iurdic:
`Waypoint 079359 - Access Restricted`.

![Entry Placard](entry-placard.png)

[TOC]


## First Floor - Environmental
<img src="waypoint-floor1.jpg"
  style="padding:0;display:block;margin:0 auto;max-height:100%;max-width:100%;">

The room inside the entry of the tower is 15 on a side. The walls are  a mass of
twisting pipes and conduits with two alcoves hollowed out within the maze and
within the alcoves are two control control banks, themselves connected to the
tangle around them. The room is damp and musty - dirt and leaves have piled
against walls and around the conduits, moldering in the puddles on the floor. At
the back of the room, a ramp wraps and spirals upward around a thick concrete
column, which has a small, open vault at its center.

### Ramp
If entered during the day, faint light can be seen reflecting down the ramp from
the open hatch to the third floor balcony.

### Control Banks
One of the control banks is responsible for regulating the environment of the
tower, including cleaning the air and water in the facility, while the other can
synthesize edible food from virtually any organic material. However long ago the
entry had been forced open, water and debris has pooled under the control banks,
and operating them may damage them, if the power can be restored at all.

### Reactor Vault
Examination reveals that this is a fundament reactor, the small chamber is domed
and lined with a synaptic nadeon array, but the fundament core is missing.
Nevertheless, the chamber still radiates alpha level curse throughout the room;
closing the vault will allow the curse to disipate from the room in a major turn.

## Second Floor - Medical
<img src="waypoint-floor2.jpg"
  style="padding:0;display:block;margin:0 auto;max-height:100%;max-width:100%;">

The ramp leads up and into another cramped room 15 feet square. Inside the curve
of the ramp, a spiral staircase leads upward. This room also has an alcove on
one side wall, holding a glassy capsule atop a metalic platform; it looks large
enough that it could hold a person. Beside it, embedded in the wall, is glass a
panel. The other side wall has a door, inside which is a small restroom with a
shower stall. On the wall opposite the ramp, a ladder rises to a hatch, open to
the outdoors, allowing debris from outside to gather in the room. Though the air
is musty, the breeze through the trap door helps to clear the air a bit.


### Recovery Bed
When powered, the panels on the walls will display diagnostic data for a patient
inside the bed and, in conjunction with the environmental system, a recovery bed
will suffuse the air inside with healing medicines. When resting inside for two
major turns to heal CON damage, roll three times, instead of twice, and discard
the lowest result. The bed can also function for an Irthmade with an appropriate
metalic slurry, allowing them to recover, rolling a CON die per two major turns
of rest inside.

Once power is restored, the beds will need their various systems flushed
thoroughly before they can be used again.

### Ladder
The ladder leads to a narrow, unrailed balcony on the building's exterior,
covered in moss and vines.

## Third Floor - Living Space
<img src="waypoint-floor3.jpg"
  style="padding:0;display:block;margin:0 auto;max-height:100%;max-width:100%;">
Ascending to the third floor, the stench of amonia fills the air and everything
is covered in grime. Again, each wall has an alcove, but these each hold the
metal frame of a bunkbed, though the bedding was long ago carried off by some
animal or another. At the end of the narrow room, there are the remains of a
lounge, the furniture stripped to their frames, and a small kitchenette with a
sink, cooking appliance and cabinets.

### Kitchenette
The appliances here are sturdy and, other than the sink, don't require much more
than cleaning and power to be operable again.

## Fourth Floor - Observation and Telemetry
The staircase continues to another floor above, the steps slick with grime. This
floor is home to a large swarm of bats, and their guano has coated everything
insde. The only visible features in the room are two large tables and the stairs
that continue upward.

### Mapping and Linking Tables
Under the guano, are two large glass topped tables and a large glass panel
embedded in the wall opposite the stairs.

The mapping table can be used to scan the area surrounding the tower, which can
be displayed on the its surface as well as projected to the display on the wall.
Though the table is covered in filth, it is in otherwise good repair.

The linking table is the waypoint's interface to the portals between the other
towers. It can register new users and answer queries about the links themselves.
Informatin can be displayed on the table's surface and projected to the wall
display. The guano has seeped into the components of the table, and although the
machinery can be repaired, the datasbase is unrecoverably corrupted.

## Fifth Floor - Portal
This room is empty, featuring only an arch at the other end of the room, a
shattered glass dome on the ceiling with ancient, damaged machinery inside, and
the stairs that rise from the fourth floor. The air is moist and foul and the
stench of the guano is overpowering. A gargoyle has made its lair here at the
top of the tower, sharing the space with another swarm of bats.

### The Gargoyle's Treasures
The gargoyle has collected durable bits and bobs and placed them around the room
in an auraly pleasing pattern and coins and other bits of metal are plastered to
the walls with guano. If any of its trophies are taken or harmed, the gargoyle
will defend them jealously.

### Portal Projector
<img src="waypoint-portal.jpg"
  style="padding:0;display:block;margin:0 auto;max-height:100%;max-width:100%;">

The shattered machinery in the ceiling is resposnible for projecting the portal
that links the towers, and must be programmed using the linking table on the
fourth floor. When the portal opens, the space under the arch at the end of room
shimmers and bends as the fabric of reality is forced to allow passage between
the distant locations. The components are damaged beyond repair and will require
replacements.

The portal can also be opened to a pocket dimension, suitable for storage.
Creatures may enter the space while the portal is open, but it is otherwise
inhospitable to life.
