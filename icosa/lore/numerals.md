# Icosan Numerals
The Icosan numeral system is a base-20 number system, where each digit features
the face of the mighty Icosatron and binary markings.

<!--![Icosan Numerals](numerals.jpg)-->
<img src="numerals.jpg" width="100%">

## Fonts
The digits 0-9 use the normal digit keys; to type 10-19, use SHIFT+digit (e.g., 17 is SHIFT+7).
* [icosan-numerals.ttf](Icosan_numerals-Regular.ttf)
* [icosan-numerals.otf](Icosan_numerals-Regular.otf)
