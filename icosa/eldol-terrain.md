# ICOSA Eldol Region Terrain Generation

## Procedure

Adapted from [Gnome Stew's Randomized Hex Crawl Generator](https://gnomestew.com/a-randomized-hex-crawl-generator/),
this procedure randomly generates the basic terrain for each hex in the Eldol region.

1. Major hexes are assigned a terrain type in accordance with the descriuption
of their region.
2. For each minor hex, determine what terrain will act as its base:
* 30% chance to use the major hex as the base terrain; if the minor hex is on
the border between two major hexes, select one at random.
* 70% chance to use a neighboring minor hex that has had its terrain assigned,
select one at random; if no minor hex neighbors are assigned yet, fallback to
the major hexes for the base terrain.
3. Roll a D12 for each table below, using the column corresponding to the
region the hex is in, and the group of rows corresponding to the base terrain
type to generate the terrain type of the minor hex.


## Major Hex Terrain Assignments By Region

The River Iyoh region are those hexes comprising the river itself and several
miles from its shores; The Shallows are the regions of Eldol on the north side
of the River Iyoh and a few miles past the southern reaches of the River Iyoh
region, before giving way to The Deeps, the southern most region of the forest.

| Region     | Elevation | Vegetation | Water |
| ---------- | --------- | ---------- | ----- |
| River Iyoh | plains    | forest     | river |
| Shallows   | hills     | forest     | none  |
| Deeps      | lowlands  | forest     | none  |


## Minor Hex Terrain Tables

 | Elevation | D12 | source    | River Iyoh | Shallows  | Deeps     |
 | --------- | --- | --------- | ---------- | --------- | --------- |
 | mountains | 1   | mountains | mountains  | mountains | mountains |
 |           | 2   | mountains | mountains  | mountains | mountains |
 |           | 3   | mountains | hills      | mountains | hills     |
 |           | 4   | mountains | hills      | mountains | hills     |
 |           | 5   | mountains | hills      | hills     | hills     |
 |           | 6   | mountains | hills      | hills     | hills     |
 |           | 7   | hills     | hills      | hills     | hills     |
 |           | 8   | hills     | hills      | hills     | hills     |
 |           | 9   | hills     | hills      | hills     | hills     |
 |           | 10  | hills     | hills      | hills     | hills     |
 |           | 11  | hills     | hills      | hills     | hills     |
 |           | 12  | hills     | hills      | hills     | hills     |
 | hills     | 1   | mountains | mountains  | mountains | mountains |
 |           | 2   | mountains | hills      | mountains | mountains |
 |           | 3   | mountains | hills      | hills     | hills     |
 |           | 4   | mountains | hills      | hills     | hills     |
 |           | 5   | hills     | hills      | hills     | plains    |
 |           | 6   | hills     | hills      | hills     | plains    |
 |           | 7   | hills     | plains     | hills     | plains    |
 |           | 8   | hills     | plains     | hills     | plains    |
 |           | 9   | plains    | plains     | plains    | plains    |
 |           | 10  | plains    | plains     | plains    | plains    |
 |           | 11  | plains    | plains     | plains    | plains    |
 |           | 12  | plains    | plains     | plains    | plains    |
 | plains    | 1   | hills     | hills      | hills     | hills     |
 |           | 2   | hills     | hills      | hills     | hills     |
 |           | 3   | hills     | hills      | hills     | plains    |
 |           | 4   | plains    | plains     | hills     | plains    |
 |           | 5   | plains    | plains     | hills     | plains    |
 |           | 6   | plains    | plains     | hills     | plains    |
 |           | 7   | plains    | plains     | plains    | plains    |
 |           | 8   | plains    | plains     | plains    | lowlands  |
 |           | 9   | plains    | plains     | plains    | lowlands  |
 |           | 10  | lowlands  | lowlands   | lowlands  | lowlands  |
 |           | 11  | lowlands  | lowlands   | lowlands  | lowlands  |
 |           | 12  | lowlands  | lowlands   | lowlands  | lowlands  |
 | lowlands  | 1   | plains    | plains     | plains    | plains    |
 |           | 2   | plains    | plains     | plains    | plains    |
 |           | 3   | plains    | plains     | plains    | plains    |
 |           | 4   | plains    | plains     | plains    | plains    |
 |           | 5   | lowlands  | lowlands   | lowlands  | lowlands  |
 |           | 6   | lowlands  | lowlands   | lowlands  | lowlands  |
 |           | 7   | lowlands  | lowlands   | lowlands  | lowlands  |
 |           | 8   | lowlands  | lowlands   | lowlands  | lowlands  |
 |           | 9   | valleys   | valleys    | valleys   | valleys   |
 |           | 10  | valleys   | valleys    | valleys   | valleys   |
 |           | 11  | valleys   | valleys    | valleys   | valleys   |
 |           | 12  | valleys   | valleys    | valleys   | valleys   |
 | valleys   | 1   | lowlands  | lowlands   | lowlands  | lowlands  |
 |           | 2   | lowlands  | lowlands   | lowlands  | lowlands  |
 |           | 3   | lowlands  | lowlands   | lowlands  | lowlands  |
 |           | 4   | lowlands  | lowlands   | lowlands  | lowlands  |
 |           | 5   | lowlands  | lowlands   | lowlands  | lowlands  |
 |           | 6   | lowlands  | lowlands   | lowlands  | lowlands  |
 |           | 7   | valleys   | valleys    | valleys   | valleys   |
 |           | 8   | valleys   | valleys    | valleys   | valleys   |
 |           | 9   | valleys   | valleys    | valleys   | valleys   |
 |           | 10  | valleys   | valleys    | valleys   | valleys   |
 |           | 11  | valleys   | valleys    | valleys   | valleys   |
 |           | 12  | valleys   | valleys    | valleys   | valleys   |


 | Vegetation | D12 | source    | River Iyoh | Shallows  | Deeps     |
 | ---------- | --- | --------- | ---------- | --------- | --------- |
 | dense      | 1   | dense     | dense      | dense     | dense     |
 |            | 2   | dense     | dense      | dense     | dense     |
 |            | 3   | dense     | forest     | dense     | dense     |
 |            | 4   | dense     | forest     | dense     | dense     |
 |            | 5   | dense     | forest     | forest    | dense     |
 |            | 6   | dense     | forest     | forest    | dense     |
 |            | 7   | forest    | forest     | forest    | dense     |
 |            | 8   | forest    | forest     | forest    | dense     |
 |            | 9   | forest    | forest     | forest    | dense     |
 |            | 10  | forest    | forest     | forest    | forest    |
 |            | 11  | forest    | forest     | forest    | forest    |
 |            | 12  | forest    | forest     | forest    | forest    |
 | forest     | 1   | dense     | dense      | dense     | dense     |
 |            | 2   | dense     | forest     | dense     | dense     |
 |            | 3   | dense     | forest     | forest    | dense     |
 |            | 4   | dense     | forest     | forest    | dense     |
 |            | 5   | forest    | forest     | forest    | dense     |
 |            | 6   | forest    | forest     | forest    | dense     |
 |            | 7   | forest    | forest     | forest    | dense     |
 |            | 8   | forest    | forest     | forest    | forest    |
 |            | 9   | grassland | grassland  | grassland | forest    |
 |            | 10  | grassland | grassland  | grassland | forest    |
 |            | 11  | grassland | grassland  | grassland | grassland |
 |            | 12  | grassland | grassland  | grassland | grassland |
 | grassland  | 1   | forest    | forest     | forest    | forest    |
 |            | 2   | forest    | forest     | forest    | forest    |
 |            | 3   | forest    | forest     | forest    | forest    |
 |            | 4   | grassland | forest     | forest    | forest    |
 |            | 5   | grassland | grassland  | forest    | forest    |
 |            | 6   | grassland | grassland  | forest    | forest    |
 |            | 7   | grassland | grassland  | forest    | forest    |
 |            | 8   | grassland | grassland  | forest    | forest    |
 |            | 9   | grassland | grassland  | grassland | grassland |
 |            | 10  | scrubland | grassland  | grassland | grassland |
 |            | 11  | scrubland | scrubland  | scrubland | scrubland |
 |            | 12  | scrubland | scrubland  | scrubland | scrubland |
 | scrubland  | 1   | grassland | grassland  | grassland | grassland |
 |            | 2   | grassland | grassland  | grassland | grassland |
 |            | 3   | grassland | grassland  | grassland | grassland |
 |            | 4   | grassland | grassland  | grassland | grassland |
 |            | 5   | scrubland | grassland  | grassland | grassland |
 |            | 6   | scrubland | grassland  | grassland | grassland |
 |            | 7   | scrubland | scrubland  | scrubland | scrubland |
 |            | 8   | scrubland | scrubland  | scrubland | scrubland |
 |            | 9   | barren    | scrubland  | scrubland | scrubland |
 |            | 10  | barren    | scrubland  | scrubland | scrubland |
 |            | 11  | barren    | scrubland  | scrubland | scrubland |
 |            | 12  | barren    | barren     | barren    | barren    |
 | barren     | 1   | scrubland | scrubland  | scrubland | scrubland |
 |            | 2   | scrubland | scrubland  | scrubland | scrubland |
 |            | 3   | scrubland | scrubland  | scrubland | scrubland |
 |            | 4   | scrubland | scrubland  | scrubland | scrubland |
 |            | 5   | scrubland | scrubland  | scrubland | scrubland |
 |            | 6   | scrubland | scrubland  | scrubland | scrubland |
 |            | 7   | barren    | scrubland  | scrubland | scrubland |
 |            | 8   | barren    | scrubland  | scrubland | scrubland |
 |            | 9   | barren    | scrubland  | scrubland | scrubland |
 |            | 10  | barren    | scrubland  | scrubland | scrubland |
 |            | 11  | barren    | barren     | barren    | barren    |
 |            | 12  | barren    | barren     | barren    | barren    |


 | Water | D12 | source  | River Iyoh | Shallows | Deeps |
 | ----- | --- | ------- | ---------- | -------- | ----- |
 | lake  | 1   | lake    | lake       | lake     | lake  |
 |       | 2   | lake    | lake       | lake     | lake  |
 |       | 3   | river   | river      | river    | river |
 |       | 4   | river   | river      | river    | river |
 |       | 5   | river   | river      | river    | none  |
 |       | 6   | river   | river      | none     | none  |
 |       | 7   | river   | river      | none     | none  |
 |       | 8   | none    | none       | none     | none  |
 |       | 9   | none    | none       | none     | none  |
 |       | 10  | none    | none       | none     | none  |
 |       | 11  | none    | none       | none     | none  |
 |       | 12  | none    | none       | none     | none  |
 | river | 1   | lake    | lake       | lake     | lake  |
 |       | 2   | lake    | lake       | lake     | lake  |
 |       | 3   | lake    | lake       | lake     | lake  |
 |       | 4   | lake    | lake       | lake     | river |
 |       | 5   | river   | river      | river    | river |
 |       | 6   | river   | river      | river    | none  |
 |       | 7   | river   | river      | river    | none  |
 |       | 8   | river   | river      | none     | none  |
 |       | 9   | none    | none       | none     | none  |
 |       | 10  | none    | none       | none     | none  |
 |       | 11  | none    | none       | none     | none  |
 |       | 12  | none    | none       | none     | none  |
 | none  | 1   | lake    | lake       | lake     | lake  |
 |       | 2   | lake    | lake       | lake     | river |
 |       | 3   | river   | river      | river    | none  |
 |       | 4   | river   | river      | none     | none  |
 |       | 5   | none    | none       | none     | none  |
 |       | 6   | none    | none       | none     | none  |
 |       | 7   | none    | none       | none     | none  |
 |       | 8   | none    | none       | none     | none  |
 |       | 9   | none    | none       | none     | none  |
 |       | 10  | none    | none       | none     | none  |
 |       | 11  | none    | none       | none     | none  |
 |       | 12  | none    | none       | none     | none  |
