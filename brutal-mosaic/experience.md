# Through Triaumph and Adversity, Enlightenment


<img src="imgs/experience.jpg"
  style="padding:0;display:block;margin:0 auto;max-height:100%;max-width:100%;">


**This module is Mosaic Strict**


Success and failure are the greatest instructors; to reach a higher level of
understanding, one must experience both to draw lessons from. However, one
cannot learn if there is neither hope for victory nor any chance of defeat. One
must also know whether they have succeeded or failed - if the outcome of some
action is unknown at the time, experience cannot be added until the result is
known.

Create a track with a number of slots equalling the number of experience points
needed to rank up an ability. When attempting an action has a reasonable chance
of both success and failure, fill one of the slots at one end with an `S` on a
success; conversely, for a failure, mark an `F` from the other end.

When the track is full and is balanced between 40-60 percent failures and
successes, the ability may increase in rank. If the track is full but not
properly balanced, successes and failures will replace each other, from the
middle, until it is. When the ability levels up, clear the track.

Actions whose outcomes are not immediately clear may be noted by the player, and
when they know the result, they may make the appropriate mark.


<img src="imgs/experience-track.jpg"
  style="padding:0;display:block;margin:0 auto;max-height:100%;max-width:100%;">


## Adjusting the Learning Rate
- The amount of experience can easily be adjusted to control the speed at which
characters increase in power.
- The balance between successes and failures can be adjusted to incentivize risk
taking (requiring more failures) versus easy gains (requiring more successes).
- The frequency at which actions can be attempted might affect how often it
could earn experience, perhaps only being able to accrue a single success or
failure per encounter, rather than for every swing of a sword.
