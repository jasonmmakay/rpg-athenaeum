# The Three Fates

<img src="imgs/resolution.jpg"
  style="padding:0;display:block;margin:0 auto;max-height:100%;max-width:100%;">

**This module is Mosaic Strict**

**This module is a hack of Iron Core**

All actions have three aspects to them: `cost`, `time` and `quality`. Whenever
you attempt an action, you will prioritize these three aspects in order of
importance to you at the time: `primary`, `secondary` and `tertiary`; then, roll
three ten-sided dice and assign the largest result to your primary aspect, the
middle result to the secondary, and the smallest to the tertiary.

The `quality die` will determine whether your action succeeds, by meeting or
beating a threshold. The threshold could be static and set by fiat, or it could
be the cost result of an opposing character's roll. If the result is 5 greater
than the threshold, the result is a critical success.

The `cost die` governs whether you can avoid unfortunate complications. If
you are actively defending against an opponent's action, the cost result will
be your opponent's threshold for success.

When the precise timing and order of events aren't as important, the `time die`
will inform the judge's decision on how long it takes to attempt an action. See
below for when timing and order *are* important.

If there are any modifiers, add them to the results. Each die may have separate
modifiers, depending on both its prioritization and which aspect it represents
(i.e. the secondary die may have a modifier and the time aspect may have a
modifier). Multiple modifiers stack.


## Expertise and Outside Influences
It's easier to succeed when you know what you're doing - each level of expertise
grants increasing bonuses to each of the dice in your roll.

Other outside factors may also help or hinder actions, such as crafting with
exceptional materials, watching for unseen enemies on a foggy night, or selling
goods in a new market. These modifiers are applied to the quality, cost or time
dice depending on which makes sense given the context of the situation; if
that's ambiguous, the player chooses how to apply bonuses and the judge decides
for penatlies.

Furthermore, while there is no limit to the amount of misfortune that could
hinder your actions, the bonuses you may take advantage of, aside from your
expertise bonus, are limited by your level of expertise.

| Expertise    | Primary | Secondary | Tertiary | Max Bonuses |
| :----------- | :-----: | :-------: | :------: | :---------: |
| Untrained    |   +0    |     +0    |    +0    |     +1      |
| Novice       |   +1    |     +0    |    +0    |     +2      |
| Trained      |   +1    |     +1    |    +0    |     +3      |
| Professional |   +2    |     +1    |    +0    |     +4      |
| Expert       |   +2    |     +1    |    +1    |     +5      |
| Master       |   +3    |     +2    |    +1    |     +6      |


## Conjunction
When the quality die shows the same result as one of the other dice, a minor
boon or complication occurs depending on whether the action failed or succeeded:
a silver lining that softens the blow of a loss or a blemish upon triumph.

If all three dice show the same result, the action succeeds, regardless of the
difficulty.


## From Chaos, Order
When timing is critical, all parties act in `initiative order`. Whenever an
actor takes an action, subtract their time result from 15, to a minimum of 1,
and add it to their current initiative result to determine the next time they
can act.

When entering into initiative order, all actors start with an initiative result
of 0 and take an initiative only action to determine when they will first act
and what their defense will be until then. Depending on whether they are
suprised, determines how they must allocate their dice.

| Suprised? | Primary | Secondary | Tertiary |
| :-------- | :-----: | :-------: | :------: |
| Yes       |    -    |   Time    |   Cost   |
| No        |   Time  |   Cost    |    -     |
