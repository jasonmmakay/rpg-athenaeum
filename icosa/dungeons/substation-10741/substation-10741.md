# SUBSTATION 10741
These are the ruins of a power generating substation, now the lair of a
dangerous megapede.

[TOC]

![Substation 10741](substation-10741.jpg)
Map created using [Watabou's One Page Dungeon Generator](https://watabou.itch.io/one-page-dungeon).

## EXTERIOR
You see before you three hills, almost perfectly dome-like, and seemingly
precise distances from each other. From a moss choked crevice, you see the
faint flickering of a neon blue light. The ground leading into the unnatural
cavern shows signs of animal activity.

- The tracks leading into cavern are of some sort of predator; a critical
success identifies them as the tracks of a megapede.
- The megapede hunts at night and lairs in the day. If the party camps nearby,
they may become the megapede's prey.
- Those with sharp senses will note a very faint tang of ozone in the air near
the crevice; a critical success will reveal it as the distinct scent found in
cursed places.
- The hollow between the hills may serve as a good place for a camp, the hills
blocking sightlines into the hollow.
- Beside the entrance, covered in centuries of dirt and growth, is a plaque
which reads "SUBSTATION 10741" in Old High Iurdic, that may be revealed by a
thorough search.


## ROOM 1 - ENTRY
The tunnel leads down into a flooded chamber. There is a neon blue light
fixture flickering faintly in the entrance, providing only enough light to show
the stairway you've just descended, and a neon purple light emanates from a
hallway to the left. The rest of the chamber is lost in darkness, and an
occasional drip from the ceiling is the only thing that breaks the silence or
the stillness of the pool's surface.

- The water in this chamber is knee deep with uncertain footing, making
movement more difficult. It emits alpha level curse (-/1/D12) to anyone within
it. Unless some method of detection is used, no one is aware of the curse until
curse symptoms become evident; the interpreter should accumulate curse in
secret until players discover the source of their symptoms.
- Other light fixtures line the center of the ceiling, un-powered.
- The door to the left leads to [the supplies storage](#room-2-supplies). It
is damaged, and permanently recessed. Through the purple-lighted hallway lies a
similar door as to the right, though it is heavily corroded by the water
reaching it from the main chamber.
- The door to right leads to [the control room](#room-3-control-room). It is
still in apparently good repair, the bottom of the door is only slightly
tarnished. It will slide open when any life form as large or larger than a
person approaches, the only sound of its opening is the quiet rush of water
moving to fill vacated space.
- The door opposite the entrance leads to [the generator](#room-4-the-generator).
It appears to be that of a vault of some sort; the metal of the door is
untarnished by the water or time and bears an inscription in Old High Iurdic:
"DANGER - PPE REQUIRED". The wheel will not turn unless the lock is disengaged
in the control room.


## ROOM 2 - SUPPLIES
The door to this room grinds grudgingly into its recess, emitting a horrific
squeal of metal as it does, echoing through the ruins. The interior of the
domed room is bathed in flickering, neon purple light; tumbled and battered
storage lockers contain the remains of supplies, rotted by the dampness and
covered in fungus, though one locker remains upright and intact. Scattered
through the rest of the debris and fungus glass vials glint in the unsteady
light.

- The squeal of the door moving will draw the attention of the megapede that
lairs in the ruins. If it is currently in its nest in the control room, it
attacks shortly after being awoken from its slumber; otherwise, it arrives in
D4 minor turns returning from its hunt.
- The upright locker contains an intact suit of Heavy Hazard(Curse) Armor.
- The rest of the loot in this room is obviously visible, but scattered beneath
the fungus: Zealium, Clementex(2), 13x Curse-prevention caplets, Curse Detector
(damaged), Mechanic's Toolbox, 2x power cells, 16x high quality metal (suitable
for Irthmade repair).
- If disturbed while searching the wreckage, the fungus disperses poisonous
spores; anyone in the area must pass a CON check or become poisoned, taking D4
poison damage for each of D4 major turns. Each turn, the save can be
re-attempted. Each time the spores are inhaled increases the difficulty of the
check and the turn duration by 1.
- One of the tumbled lockers has a digital code scrawled on the inside of the
door in Old High Iurdic, though some of it is illegible: "87?810?5". This will
only be found if a thorough search of the room is undertaken.


## ROOM 3 - CONTROL ROOM
This domed room is shrouded in darkness. At its center is the macabre nest of
some beast: with a diameter of some 10 feet, it is a pile of damp earth,
moldering wood and bones, some obviously humanoid. A battered desk is shunted
against back wall, behind the nest, above which is a button panel.

- Once the control office for the substation, a megapede has made this room its
nest. The megapede lairs in its nest during the day and hunts by night.
- The battered desk against the wall holds a still functioning tablet, though
the power cell has long since discharged. When powered on, one will find that
it was the last attendant's PDA, and the simple AI built into it will cheerily
request a password, though it might be convinced to allow access without it. If
access can be gained, maintenance records for the substation, including
historical power output statistics, and the passcode to unlock the vault can be
found within. The records indicate other such substations once existed in the
region, powering something called 'The Center'. Otherwise, the device is
installed with standard office applications, though all are currently in Old
High Iurdic.
- The remains of several ruiners can be found in the nest. Each of 4 minor
turns spent sifting through the nest will turn up D2-1 gold plasters and D100
silver plasters. The recognizable equipment is mostly damaged beyond repair by
the damp or the megapede's mandibles, except for a [damaged revolver](#damaged-revolver),
still strapped to the skeleton that formerly owned it.
- The panel unlocks the core vault. It has 10 buttons on it, each labeled with
the Old High Iurdic digits, 0-9. Close examination reveals that the digits 3
and 8 have more wear than the others, while 0, 1, 5, and 7 have only slight
wear. The code is "87381035". If the wrong code is input, the spaces between
the keys will flash red; a second incorrect input cauases a klaxon to sound
throughout the facility, which will draw the megapede back to its lair in D3
minor turns, if it is not there.


## ROOM 4 - THE GENERATOR
When the vault door to the generator is opened, the air is suffused with the
stench of ozone, the taste of copper and a low hum; an eerie green glow
emanates from a block of metal that rests on a raised plinth in the center of
the room. Exposed skin tingles, reddens and burns immediately under exposure
to the light. The block of metal, though solid, shimmers in a sickly, dark
rainbow of colors, like oil on the surface of water ([fundament core](#fundament-core)).
The ceiling and walls of the domed chamber are covered in uncountable small,
metal plates ([synaptic nadion array](#synaptic-nadion-array)), and it is
apparent that the water flooding the entry has seeped around the vault door
into the chamber.

- When the core vault is opened, gamma level curse (D8/2D6/4D12) floods into
the entry chamber, and beta level curse (2/D4/2D12) into the supply and control
rooms; for each closed hallway door that is between the open vault and a
character, the curse level is reduced one step; characters within close
distance to the ruins but outside them suffer alpha level curse (-/1/D12)
exposure; closing the vault door returns curse levels to their previous levels
after a major turn.
- The fundament core is not attached to its plinth, anyone mad enough to pick
it up may.


## MEGAPEDE
Megapedes have the appearance of common centipedes, only enlarged to gargantuan
proportions: megapedes may reach sizes up to 12 feet long. Known for their
quickness, the insects are carniverous, biting their prey and injecting them
with poison, often dragging the corpse back to its nest.

**Attributes**
- CON D8 (40) // ATN D4 (8)
- AV 4 // PAV 0 // CAV 1

**Skills**
- Predatory Hunter D8
- Dodge D6

**Signatures**
- Fast: The megapede can move a near or close distance without limitation, or
far by sacrificing its action.
- Heat/Fire/Dryness Weakness: Megapedes lack the waxy coating that many other
insects have, and they can quickly desicate if exposed to extreme heat or
dryness.
- Bludgeoning Resistence: AP negated from bludgeoning attacks.
- Brittle Carapace: bludgeoning damage will crack the carapace; a targeted
bludgeoning strike on a cracked spot will break the carapace open; all targeted
strikes at the open carapace are then at AV 0 and bludgeoning resistance is
negated in that spot.

**Attacks**
- Venomous bite: 2D4 piercing AP2 + D4 poison / D4 rounds CON save


## ITEMS

### Damaged Revolver
It still holds 3 chambered bullets, though they will misfire catastrophically
on a roll of a 1 or 2. If new ammunition is used, the revolver will only
misfire on a roll of a 1. If repaired and good ammunition is used, the weapon
is fine.

### Fundament Core
Measuring just 8.25 inches per side, it is very dense, having EV 20. It
shimmers in a sickly, dark rainbow of colors, like oil on the surface of water
and it emits gamma level curse.

### Synaptic Nadion Array
These small metal panels absorb curse and convert it to usable energy.
