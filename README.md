# Icosa

## Lore
- [Bestiary](icosa/bestiary.md)
- [Numerals](icosa/lore/numerals.md) ([.ttf](icosa/lore/Icosan_numerals-Regular.ttf) / [.otf](icosa/lore/Icosan_numerals-Regular.otf))
- [Calendar](icosa/lore/calendar.md)

## Maps
- [Eldol](icosa/maps/eldol.jpg) ([.WonderDraft](icosa/maps/eldol.wonderdraft_map))
- [Big Rock Region](icosa/maps/big-rock-region.jpg) ([.WonderDraft](icosa/maps/big-rock.wonderdraft_map))

## Dungeons
- [Substation 10741](icosa/dungeons/substation-10741/substation-10741.md)

## Articles
- [Eldol Region Terrain Generation](icosa/eldol-terrain.md)


# Telesstia
## Maps
- [Teleste](telestia/maps/teleste.jpg) ([.WonderDraft](telestia/maps/teleste.wonderdraft_map))
- [Telestia](telestia/maps/telestia.jpg)
- [Bluffield](telestia/maps/bluffield.jpg) ([.WonderDraft](telestia/maps/bluffield.wonderdraft_map))
- [The Copse](telestia/maps/the-copse.jpg)
- [The Cragwood](telestia/maps/cragwood.jpg)
- [Veron's End](telestia/maps/verons-end.jpg)


# Eberron
## Maps
- [Trebaz Sinara](eberron/maps/TrebazSinara.jpg) ([.WonderDraft](eberron/maps/TrebazSinara.wonderdraft_map))


# Procgen Resources
## Hex Crawl
- Overworld terrain type
  - [A Randomized Hex Crawl Generator](https://gnomestew.com/a-randomized-hex-crawl-generator/)
- Forest trail and point of interest locations
  - [Into the Wyrd and Wild](https://www.drivethrurpg.com/product/274922/Into-the-Wyrd-and-Wild-Revised-Edition)
- Flora and Fauna
  - [European Style Forest Generator](https://www.drivethrurpg.com/product/250737/European-Style-Forest-Generator)
  - [Trees for All Your Needs](https://www.drivethrurpg.com/product/55807/Two-Bit-Tables-Trees-for-All-Your-Needs)
  - [100 Temperate Forest Plants](https://www.drivethrurpg.com/product/23852/100-Temperate-Forest-Plants)
  - [100 Wilderness Features And Landmarks](https://www.drivethrurpg.com/product/23327/100-Wilderness-Features-And-Landmarks)
  - [Mundane Cave Animal Encounters](https://www.drivethrurpg.com/product/211296/Mundane-Cave-Animal-Encounters)
  - [Mundane Forest Animal Encounters](https://www.drivethrurpg.com/product/55808/Two-Bit-Tables-Mundane-Forest-Animal-Encounters)
  - [Mundane Swamp Animal Encounters](https://www.drivethrurpg.com/product/105843/Two-Bit-Tables-Mundane-Swamp-Animal-Encounters)

## Artifact Generation
- [Quick Generator - SciFi Objects](https://www.drivethrurpg.com/product/180338/Quick-Generator--SciFi-Objects)
- [Technobabble Generator - Expanded](https://www.drivethrurpg.com/product/120739/Technobabble-Generator--Expanded)


# Public Domain
## Images
[Picryl](picryl.com)
